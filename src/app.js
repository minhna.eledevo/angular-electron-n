import { app, BrowserWindow, ipcMain } from "electron/main";
import path from "path";
import jetpack from "fs-jetpack";
import electronDl, { download } from "electron-dl";

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    // show: false,
    // autoHideMenuBar: true,
    webPreferences: {
      // preload: path.join(app.getAppPath(), "/src/preload.js"),
      sandbox: false,
      contextIsolation: false,
      nodeIntegration: true,
    },
  });
  mainWindow.webContents.openDevTools();
  // mainWindow.loadFile('index.html')
  mainWindow.loadURL("http://localhost:4200/");

  jetpack.dir("AppData");
  const folderCurrent = jetpack.path("AppData");

  mainWindow.on("ready-to-show", () => {
    mainWindow.show();
    ipcMain.on("download-file", async (_e, { downloadUrl, fileName }) => {
      const thisWindow = BrowserWindow.getFocusedWindow();
      try {
        const options = {
          directory: folderCurrent,
          filename: fileName,
        };
        const downloadItem = await download(thisWindow, downloadUrl, options);
        const savePath = downloadItem.getSavePath();
        console.log("🚀 ~ ipcMain.on ~ savePath:", savePath);
      } catch (error) {
        if (error instanceof electronDl.CancelError) {
          console.info("item.cancel() đã được gọi");
        } else {
          console.error(error);
        }
      }
    });
  });
}

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
