import { Component } from '@angular/core';
// import { ipcRenderer } from 'electron'
import { } from 'electron'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'angular-demo-n';
  // ipcRenderer!: typeof ipcRenderer;


  constructor() {
    // this.ipcRenderer = (window as any).require('electron').ipcRenderer;
  }

  onClickButton = async (): Promise<void> => {
    const fileUrl = 'https://d3jy31tj1gt845.cloudfront.net/reactdigitalgarden/json/menu.json';
    const random = Math.floor(Math.random() * 1000).toString();
    // Send a download request to the main process
    window.require('electron').ipcRenderer.send('download-file', { downloadUrl: fileUrl, fileName: `${random}.json` });

    // this.ipcRenderer.send('download-file', { downloadUrl: fileUrl, fileName: `${random}.json` });
  }

}
